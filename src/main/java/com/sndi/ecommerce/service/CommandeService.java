package com.sndi.ecommerce.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.Acheteur;
import com.sndi.ecommerce.entities.CommandItem;
import com.sndi.ecommerce.entities.Commande;
import com.sndi.ecommerce.entities.StatutLivraison;
import com.sndi.ecommerce.repositories.CommandeRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommandeService implements ICommandeServiceClient {
  private final CommandeRepository repo;
  private final UserService userService;
  private final ArticleService articleService;

  public Commande findCommande(Long id) {
    return repo.findById(id).orElse(null);
  }

  private Commande createCommande(DtoCommande dto) {
    Commande commande = prepareCommande(dto, new Commande());

    return repo.save(commande);
  }

  public Commande updateCommande(long idCommande, DtoCommande dto) {
    try {
      Commande old = repo.findById(idCommande).orElse(null);
      if (old != null)
        old.getCommandItems().clear();
      Commande commande = prepareCommande(dto, old);

      return repo.save(commande);
    } catch (Exception e) {

    }
    return null;
  }

  private Commande prepareCommande(DtoCommande dto, Commande commande) {
    commande.setDateCmd(new Date());
    Acheteur acheteur = (Acheteur) userService.findById(dto.idAcheteur);
    commande.setAcheteur(acheteur);
    commande.setStatut(StatutLivraison.NON_LIVREE);
    if (commande.getCommandItems() == null)
      commande.setCommandItems(new ArrayList<>());
    for (DtoItemCmd item : dto.items) {
      commande.getCommandItems().add(prepareItemCmd(item, commande));
    }
    return commande;
  }

  private CommandItem prepareItemCmd(DtoItemCmd item, Commande commande) {
    return CommandItem.builder().article(articleService.findArticle(item.idArticle)).nbre(item.nbre)
        .commande(
            commande)
        .build();
  }

  @Override
  public Commande saveCommande(DtoCommande dto) {
    log.info("CommandeService Call---- saveCommande ------");
    return createCommande(dto);
  }

  @Override
  public Commande confirmerLivraison(Long id) {
    Commande commande = findCommande(id);
    if (commande != null) {
      log.info("CommandeService Call---- confirmerLivraison ------");
      commande.setStatut(StatutLivraison.LIVREE);
      repo.save(commande);
    }
    return commande;
  }

  @Override
  public List<Commande> findCommandsALivrer() {
    return repo.findCommandsALivrer();
  }

}
