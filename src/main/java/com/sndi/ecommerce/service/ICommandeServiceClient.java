package com.sndi.ecommerce.service;

import java.util.List;

import com.sndi.ecommerce.entities.Commande;

public interface ICommandeServiceClient {
    public Commande saveCommande(DtoCommande dto);

    public Commande findCommande(Long id);

    public Commande confirmerLivraison(Long id);

    public List<Commande> findCommandsALivrer();

}
