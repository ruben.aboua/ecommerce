package com.sndi.ecommerce.service;

import com.sndi.ecommerce.entities.Article;

public interface IArticleServiceClient {

    public Article saveArticle(DtoArticle dto);

    public Article findArticle(Long idArticle);
}
