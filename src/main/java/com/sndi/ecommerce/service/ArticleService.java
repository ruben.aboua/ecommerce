package com.sndi.ecommerce.service;

import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.Article;
import com.sndi.ecommerce.entities.Vendeur;
import com.sndi.ecommerce.repositories.ArticleRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class ArticleService implements IArticleServiceClient {

	private final ArticleRepository repo;
	private final UserService userService;

	public Article findArticle(Long id) {
		return repo.findById(id).orElse(null);
	}

	private Article createArticle(DtoArticle dto) {
		Article article = prepareArticle(dto, new Article());

		return repo.save(article);
	}

	public Article updateArticle(long idArticle, DtoArticle dto) {
		try {
			Article old = findArticle(idArticle);
			Article article = prepareArticle(dto, old);

			return repo.save(article);
		} catch (Exception e) {

		}
		return null;
	}

	private Article prepareArticle(DtoArticle dto, Article article) {
		article.setName(dto.name);
		article.setDescription(dto.desc);
		article.setPrice(dto.price);
		Vendeur vendeur = (Vendeur) userService.findById(dto.vendeur_id);
		article.setVendeur(vendeur);
		return article;
	}

	@Override
	public Article saveArticle(DtoArticle dto) {
		log.info("ArticleService Call---- SaveArticle ------");
		return createArticle(dto);
	}

}
