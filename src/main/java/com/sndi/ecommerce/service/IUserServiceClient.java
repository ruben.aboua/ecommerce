package com.sndi.ecommerce.service;

import com.sndi.ecommerce.entities.User;
import com.sndi.ecommerce.security.dto.SignUpForm;

public interface IUserServiceClient {

    public User createUser(SignUpForm request);

    public User findUser(Long id);

    public boolean isUserAcheter(Long idUser);

    public boolean isUserLivreur(Long idUser);

    public boolean isUserVendeur(Long idUser);

}
