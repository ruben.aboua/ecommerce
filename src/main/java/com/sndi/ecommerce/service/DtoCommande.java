package com.sndi.ecommerce.service;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DtoCommande {

    public long idCommande; 
    public long idAcheteur; 
    public DtoItemCmd [] items;

}
