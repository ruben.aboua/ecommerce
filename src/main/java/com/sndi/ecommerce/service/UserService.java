package com.sndi.ecommerce.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.Acheteur;
import com.sndi.ecommerce.entities.Livreur;
import com.sndi.ecommerce.entities.Role;
import com.sndi.ecommerce.entities.User;
import com.sndi.ecommerce.entities.Vendeur;
import com.sndi.ecommerce.repositories.UserRepository;
import com.sndi.ecommerce.security.Security;
import com.sndi.ecommerce.security.dto.SignUpForm;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class UserService implements IUserServiceClient {

    private final UserRepository repo;
    private final Security security;

    public User findUser(Long id) {
        return findById(id);
    }

    public User findById(Long id) {
        return repo.findById(id).orElse(null);
    }

    @Override
    public boolean isUserAcheter(Long idUser) {
        Acheteur user = (Acheteur) findById(idUser);
        return (user != null);
    }

    @Override
    public boolean isUserLivreur(Long idUser) {
        Livreur user = (Livreur) findById(idUser);
        return (user != null);
    }

    @Override
    public boolean isUserVendeur(Long idUser) {
        Vendeur user = (Vendeur) findById(idUser);
        return (user != null);
    }

    @Override
    public User createUser(SignUpForm request) {
        User user = null;
        Role roleSelected = Role.valueOf(request.getRole());
        if (Role.ROLE_ACHETEUR.equals(roleSelected))
            user = new Acheteur();
        if (Role.ROLE_LIVREUR.equals(roleSelected))
            user = new Livreur();
        if (Role.ROLE_VENDEUR.equals(roleSelected))
            user = new Vendeur();

        if (user != null) {
            log.info("UserService Call---- createUser ------");
            user.setName(request.getFirstName());
            user.setSurname(request.getLastName());
            user.setPhone(request.getTelephone());
            user.setPassword(security.cryptPassword(request.getPassword()));
            user.setRole(roleSelected);
            user = repo.save(user);
        }
        return user;
    }
}
