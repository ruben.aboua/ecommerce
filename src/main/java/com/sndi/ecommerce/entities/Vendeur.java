package com.sndi.ecommerce.entities;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@DiscriminatorValue(value = "V")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Vendeur extends User {

	@JsonIgnore
	@OneToMany(mappedBy = "vendeur")
	private Set<Article> articles;

}
