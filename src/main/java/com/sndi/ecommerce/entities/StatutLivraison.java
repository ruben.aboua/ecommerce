package com.sndi.ecommerce.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatutLivraison {
    NON_LIVREE("Non livrée"),
    LIVREE("Livrée");

    private String libelle;
}
