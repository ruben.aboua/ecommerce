package com.sndi.ecommerce.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Role {
    ROLE_LIVREUR("LIVREUR"),
    ROLE_ACHETEUR("ACHETEUR"),
    ROLE_VENDEUR("VENDEUR");

    private String libelle;
}
