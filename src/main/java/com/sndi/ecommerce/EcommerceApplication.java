package com.sndi.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceApplication {

	// @Autowired
	// UserRepository userRepository;
	// @Autowired
	// ArticleService articleService;

	// @Autowired
	// Environment environment;

	public static void main(String[] args) {
		SpringApplication.run(EcommerceApplication.class, args);
	}

	// @Override
	// public void run(String... args) throws Exception {

	// String activeProfile = environment.getProperty("spring.profiles.active");

	// if ("dev-h2".equals(activeProfile)) {
	// User vendeur = new Vendeur();
	// vendeur.setName("Soum");
	// vendeur.setSurname("Abou");
	// vendeur.setPhone("1234");
	// userRepository.save(vendeur);

	// User acheteur = new Acheteur();
	// acheteur.setName("Ruben");
	// acheteur.setSurname("Aboua");
	// acheteur.setPhone("4567");
	// userRepository.save(acheteur);
	// }

	// }

}
