package com.sndi.ecommerce.security.web;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

import org.springframework.validation.BindingResult;

public class HttpRequestUtil {
	private HttpRequestUtil() {
	}

	@SuppressWarnings("all")
	public static void checkBindingResult(BindingResult bindingResult) {

		if (bindingResult != null && bindingResult.hasErrors() && bindingResult.getFieldError() != null) {
			String errorMessage = bindingResult.getFieldError().getField() + " "
					+ bindingResult.getFieldError().getDefaultMessage();
			throw new ValidationException(errorMessage);
		}
	}

	public static String getAuthorization(HttpServletRequest request) {
		return request.getHeader("Authorization");
	}
}
