package com.sndi.ecommerce.security.dto;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginForm {

	@NotNull
	@NotEmpty
	private String telephone;
	@NotNull
	@NotEmpty
	private String password;
	
}
