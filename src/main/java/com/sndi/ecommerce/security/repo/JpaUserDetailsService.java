package com.sndi.ecommerce.security.repo;

import javax.persistence.EntityNotFoundException;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.User;
import com.sndi.ecommerce.security.service.UserPrinciple;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class JpaUserDetailsService implements UserDetailsService {

	private final UserJpaRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = repository.getByPhone(username);
		if (user == null)
			throw new EntityNotFoundException("User Not Found with -> username or email : " + username);

		return UserPrinciple.build(user);
	}

}
