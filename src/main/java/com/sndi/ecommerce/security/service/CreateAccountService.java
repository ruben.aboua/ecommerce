package com.sndi.ecommerce.security.service;

import org.springframework.stereotype.Service;

import com.sndi.ecommerce.entities.User;
import com.sndi.ecommerce.security.AccountCreationException;
import com.sndi.ecommerce.security.dto.SignUpForm;
import com.sndi.ecommerce.security.repo.UserJpaRepository;
import com.sndi.ecommerce.service.UserService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class CreateAccountService {

	private final UserJpaRepository accounrRepository;
	private final UserService service;

	public User createAccount(final SignUpForm request) {

		if (accounrRepository.existsByPhone(request.getTelephone())) {
			throw new AccountCreationException("Le numéro de télephone est déja utilisé par un autre utilisateur");
		}

		return service.createUser(request);
	}

}
