package com.sndi.ecommerce.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class JUtils {
    private JUtils() {
    }

    public static Date stringToDate(String strDate) {
        Date date = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
            date = formatter.parse(strDate);
            return date;
        } catch (Exception e) {
        }
        return date;

    }
}
