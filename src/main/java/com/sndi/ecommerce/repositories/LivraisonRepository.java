package com.sndi.ecommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sndi.ecommerce.entities.Livraison;

public interface LivraisonRepository extends JpaRepository<Livraison, Long> {

}
