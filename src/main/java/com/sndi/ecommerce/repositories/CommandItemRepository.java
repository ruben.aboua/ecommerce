package com.sndi.ecommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sndi.ecommerce.entities.CommandItem;

public interface CommandItemRepository extends JpaRepository<CommandItem, Long> {

}
