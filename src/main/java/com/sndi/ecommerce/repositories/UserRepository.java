package com.sndi.ecommerce.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sndi.ecommerce.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
