package com.sndi.ecommerce.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sndi.ecommerce.entities.Commande;
import com.sndi.ecommerce.service.CommandeService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasRole('LIVREUR')")
public class LivraisonController {
    private final CommandeService service;

    @PutMapping("/commandes/{idCommande}/ship")
    public Commande confirmerLivraison(@PathVariable(name = "idCommande") long idCommande) {
        return service.confirmerLivraison(idCommande);
    }

}
