package com.sndi.ecommerce.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sndi.ecommerce.entities.Article;
import com.sndi.ecommerce.service.ArticleService;
import com.sndi.ecommerce.service.DtoArticle;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/articles")
@PreAuthorize("hasRole('VENDEUR')")
public class ArticleController {

	private final ArticleService articleService;

	@GetMapping("/{idArticle}")
	public Article getArticle(@PathVariable Long idArticle) {

		return articleService.findArticle(idArticle);
	}

	@PostMapping
	public Article createArticle(@RequestBody DtoArticle dtoArticle) {
		return articleService.saveArticle(dtoArticle);
	}

	@PutMapping("/{idArticle}/edit")
	public Article updateArticle(@PathVariable(name = "idArticle") long idArticle, @RequestBody DtoArticle dtoArticle) {
		return articleService.updateArticle(idArticle, dtoArticle);
	}

}
