package com.sndi.ecommerce.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sndi.ecommerce.entities.Commande;
import com.sndi.ecommerce.service.CommandeService;
import com.sndi.ecommerce.service.DtoCommande;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/commandes")
@PreAuthorize("hasRole('ACHETEUR')")
public class CommandeController {

    private final CommandeService service;

    @GetMapping("/{idCommande}")
    public Commande getArticle(@PathVariable Long idCommande) {
        return service.findCommande(idCommande);
    }

    @PostMapping
    public Commande createCommande(@RequestBody DtoCommande dtoCommande) {
        return service.saveCommande(dtoCommande);
    }

    @PutMapping("/{idCommande}/edit")
    public Commande updateArticle(@PathVariable(name = "idCommande") long idCommande,
            @RequestBody DtoCommande dtoCommande) {
        return service.updateCommande(idCommande, dtoCommande);
    }

}
