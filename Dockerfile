FROM openjdk:17

#FROM quay.io/wildfly/wildfly
#ADD target/microprofile-config.war /opt/jboss/wildfly/standalone/deployments/

COPY target/ecommerce.jar ./ecommerce.jar

CMD ["java", "-jar", "ecommerce.jar"]

#ENTRYPOINT ["java", "-jar", "ecommerce.jar"]

EXPOSE 8080